package com.example.two_session;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Wait_timer extends AppCompatActivity {

    Button but;
   Button but1;
   TextView textView;
    int chas = 0;
    int min = 15;
    int sec = 0;
    boolean isTimerStop = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_timer);

        but = (Button) findViewById(R.id.stop);
        but1 = (Button) findViewById(R.id.cancel);
        textView = (TextView) findViewById(R.id.text);
        Timer();
    }
        //расчет
    public void Timer() {
        new CountDownTimer((chas * 3600000) + (min * 60000) + (sec * 1000), 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                sec--;
                if (sec >= 0) {
                    textView.setText(Integer.toString(chas) + ":" + Integer.toString(min) + ":" + Integer.toString(sec));
                } else {
                    min--;
                    sec = 59;
                    if (min >= 0) {
                        textView.setText(Integer.toString(chas) + ":" + Integer.toString(min) + ":" + Integer.toString(sec));
                    } else {
                        chas--;
                        min = 59;
                        textView.setText(Integer.toString(chas) + ":" + Integer.toString(min) + ":" + Integer.toString(sec));
                    }
                }
            }
                //вывод
            @Override
            public void onFinish() {
                textView.setText(Integer.toString(chas) + ":" + Integer.toString(min) + ":" + Integer.toString(sec));
            }
        }.start();
    }

    public void go(View view) {
        if (isTimerStop) {
            Timer();
            isTimerStop = false;
        }
    }
    public void st(View view) {
        finish();
        startActivity(new Intent(this, Drive_timer.class));
    }

    public void can(View view) {
        finish();
        startActivity(new Intent(this, Drive_timer.class));
    }
}
